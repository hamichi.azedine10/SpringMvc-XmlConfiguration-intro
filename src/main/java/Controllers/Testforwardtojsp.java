package Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = "/TestforjspController")
public class Testforwardtojsp {

    @GetMapping("/testjsp")
    public String test(Model model){
        List<String> s = Arrays.asList("Yacine","Azedine","Khaled","Mayas");
        model.addAttribute("liste",s);
    // renvoyer vers la page test.jsp en passant par le resolver deja configuré.
    return "test";
}

    @GetMapping("/formulaire")
    public String redirect (){
        System.out.println("ok");
        return "nameform";
    }
    @PostMapping("/affichage")
    public String testform(@RequestParam String nom, @RequestParam ("prenom") String monprenom, Model model){
        model.addAttribute("nom",nom);
        model.addAttribute("prenom",monprenom);
        return "resulttesform";
    }

}
